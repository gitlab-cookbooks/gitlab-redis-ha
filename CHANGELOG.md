#gitlab-redis-ha CHANGELOG

This file is used to list changes made in each version of the gitlab-redis-ha cookbook.

## 0.1.2

Drop chef search for cluster members and use a hash

## 0.1.1

Use gitlab-vault and updated license

## 0.1.0

Initial release of gitlab-redis-ha
