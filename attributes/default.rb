default['gitlab-redis-ha']['port'] = '6379'
default['gitlab-redis-ha']['monitor_port'] = '6380'
default['gitlab-redis-ha']['ringnumber'] = '0'
default['gitlab-redis-ha']['authkey'] = 'override_attribute in vault!'
default['gitlab-redis-ha']['redis_auth'] = 'override_attribute in vault!'
default['gitlab-redis-ha']['nodelist'] = []
# default['gitlab-redis-ha']['nodelist'] = ['127.0.0.1', '127.0.0.2']
