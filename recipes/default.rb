#
# Cookbook Name:: gitlab-redis-ha
# Recipe:: default
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
include_recipe 'gitlab-vault'
cluster_conf = GitLab::Vault.get(node, 'gitlab-redis-ha')

package 'pacemaker'

service 'corosync' do
  supports :restart => true
  action [:enable, :start]
  notifies :restart, "service[pacemaker]"
end

service 'pacemaker' do
  supports :restart => true
  action [:enable, :start]
end

template '/usr/lib/ocf/resource.d/pacemaker/gitlab_redis' do
  mode '0755'
  variables(
    monitor_port: cluster_conf['monitor_port'],
    port: cluster_conf['port'],
    redis_auth: cluster_conf['redis_auth']
  )
  notifies :restart, "service[corosync]", :delayed
end

template '/root/gitlab_redis_recovery.sh' do
  mode '0755'
  variables(
    port: cluster_conf['port']
  )
end

# Fetch local ip
local_ip = '127.0.0.1'
non_local_interface = node['network']['interfaces'].find { |k, _| k != 'lo' }
non_local_interface.last['addresses'].map do |ip, details|
    local_ip = ip if details['family'] == 'inet'
end

template '/etc/corosync/corosync.conf' do
  mode '0644'
  variables(
    ringnumber: cluster_conf['ringnumber'],
    ip: local_ip,
    nodelist: cluster_conf['nodelist']
  )
#  notifies :restart, "service[corosync]", :delayed
end

file '/etc/corosync/authkey' do
  mode '0400'
  content "#{cluster_conf['authkey']}\n"
#  notifies :restart, "service[corosync]", :delayed
end

bash 'configure crm' do
  code <<-EOH
    crm configure property stonith-enabled=false
    crm configure property no-quorum-policy=ignore
    crm configure rsc_defaults resource-stickiness=100
    crm configure primitive gitlab_redis ocf:pacemaker:gitlab_redis op monitor interval=30s
  EOH
  only_if 'crm status'
  not_if 'crm resource status gitlab_redis'
end
